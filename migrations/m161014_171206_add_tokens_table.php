<?php

use yii\db\Migration;
use yii\db\pgsql\Schema;
use yii\db\Expression;

class m161014_171206_add_tokens_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%tokens}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'device_id' => Schema::TYPE_STRING  . '(32) NULL',
            'device_name' => Schema::TYPE_STRING . '(32) NULL',
            'token' => Schema::TYPE_STRING . ' NOT NULL',
            'expires_at' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'created_at' => Schema::TYPE_TIMESTAMP,
            'updated_at' => Schema::TYPE_TIMESTAMP,
        ], null);

        $this->createIndex('idx_tokens_user_id', '{{%tokens}}', 'user_id');
        $this->createIndex('idx_tokens_token', '{{%tokens}}', 'token', true);
    }

    public function down()
    {
        $this->dropTable('{{%tokens}}');
    }

}
