<?php

namespace app\controllers;

use app\models\Categories;
use app\models\States;
use app\models\Tickets;
use app\models\Tokens;
use app\models\Users;
use yii\web\Controller;
use yii\filters\auth\HttpBearerAuth;

class UtilsController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        return $behaviors;
    }

    public function actionTicketUnread(){

        $userId = Users::getCurUserId();
        $query = Tickets::find();
        $query->joinWith(['state'], false, 'INNER JOIN');
        $query->leftJoin('ticket_reads', ['and', 'tickets.id = ticket_reads.ticket_id', "ticket_reads.user_id = $userId"]);
        $query->where(['is', 'ticket_reads.ticket_id', null]);
        $query->andWhere(['<>', 'state_id', 4]);

        $result = [
            'count' => $query->count(),
        ];
        return $result;
    }

    public function actionTicketLast(){
        return Tickets::find()->orderBy(['id' => SORT_DESC])->one();
    }

    public function actionCategories(){
        $categories = Categories::find()->all();
        return $categories;
    }

    public function actionStates(){
        return States::find()->all();
    }

    public function actionTest(){
        $userId = 129;
    }

}