<?php


namespace app\controllers;

use Yii;
use app\models\UploadForm;
use yii\web\UploadedFile;
use yii\rest\ActiveController;

class UploadController extends ActiveController
{
    public $uploadPath = '../uploads/';
    public $modelClass = 'app\models\Attachments';

    public function verbs()
    {
        $verbs = parent::verbs();
        $verbs[ "upload" ] = ['POST'];
        return $verbs;
    }

    /***
     * application/json
     * curl -v -H 'Content-Type: multipart/form-data' -H 'Accept: application/json' -H 'Authorization: Bearer xQimbMcnfDYw0pd5xFTG7ShCjcl2AdAO' -X POST http://api.suz.dev/upld/upload -F "data=@6ytn0EYayZ8.png" -F "userid=1" -F "filecomment=This is an image file"
     */

    public function actionUpload()
    {

        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstanceByName('imageFile');
            if ($model->upload()) {
                
                return [
                    ["upload" => "ok"],
                    ["file_name" => $model->imageFile->baseName . '.' . $model->imageFile->extension],
                    ["class" => $model->imageFile],
                ];
                
            }else{
                print_r($model->getErrors());
            }
        }
    }
}