<?php

namespace app\controllers;

use yii\data\ActiveDataProvider;
use app\models\Reports;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;

class ReportController extends ActiveController
{
    public $modelClass = 'app\models\Reports';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'indexDataProvider'];
        return $actions;
    }

    public function indexDataProvider()
    {

        $query = Reports::find();

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
    }
}