<?php

namespace app\controllers;
use app\models\LoginForm;
use app\models\Tokens;
use app\models\Users;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\Users';


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['authenticator']['except'] = ['login'];
        return $behaviors;
    }

    public function actions(){
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'indexDataProvider'];
        return $actions;
    }

    public function indexDataProvider()
    {
        return new ActiveDataProvider([
            'query' => Users::find(),
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ]
            ],
        ]);
    }

    public function actionView($id)
    {
        return Users::findOne($id);
    }

    public function actionProfile()
    {
        $userId = Yii::$app->user->identity->getId();
        return $this->actionView($userId);
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {

            $token = new Tokens();
            $token->user_id = Users::getCurUserId();
            $token->token = Users::generateToken();
            $token->save();

            return $token->token; //Yii::$app->user->identity->getAuthKey();
        } else {
            return $model;
        }
    }

    public function actionLogout()
    {
        //Yii::$app->user->logout();
        return Yii::$app->user->identity;
    }

}