<?php
/**
 * Created by PhpStorm.
 * User: 0810
 * Date: 24.08.2016
 * Time: 8:38
 */

namespace app\controllers;


use app\models\Tickets;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;

class TicketController extends ActiveController
{
    public $modelClass = 'app\models\Tickets';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        //$behaviors['authenticator']['only'] = ['update'];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'indexDataProvider'];
        unset($actions['view']);
        return $actions;
    }

    public function indexDataProvider()
    {
        $query = Tickets::find();
        $query->joinWith(['state'], false, 'INNER JOIN');
        $query->where(['<>', 'state_id', 4]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        /*var_dump($query->createCommand()->getRawSql());
        exit();*/
    }

    public function actionView($id)
    {
        $query =  Tickets::findOne($id)->viewTicket();

        return [
            'ticket' => $query,
            'states' => $query->states,
            'messages' => $query->messages,
            'attachments' => $query->attachments,
        ];
    }

    public function actionSearch()
    {
        if (!empty($_GET)) {
            $model = new $this->modelClass;
            foreach ($_GET as $key => $value) {
                if (!$model->hasAttribute($key)) {
                    throw new \yii\web\HttpException(404, 'Invalid attribute:' . $key);
                }
            }
            try {
                $provider = new ActiveDataProvider([
                    'query' => $model->find()->where($_GET),
                    'pagination' => false
                ]);
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(500, 'Internal server error');
            }

            if ($provider->getCount() <= 0) {
                throw new \yii\web\HttpException(404, 'No entries found with this query string');
            } else {
                return $provider;
            }
        } else {
            throw new \yii\web\HttpException(400, 'There are no query string');
        }
    }

    public function actionCount(){
        echo "cnt";
    }
}