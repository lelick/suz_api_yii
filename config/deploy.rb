# config valid only for current version of Capistrano
lock '3.6.1'

set :username, 'deploy'
set :application, 'suz_api'
set :repo_url, 'git@bitbucket.org:lelick/suz_api_yii.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :deploy_to, '/home/deploy/api'

set :log_level, :info

set :rvm_ruby_version, '2.3.1'      # Defaults to: 'default'

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

set :linked_files, %w(config/db.php config/params.php web/index.php)
set :linked_dirs, %w(log runtime vendor uploads web/assets)

namespace :setup do
  desc 'upload files on server'
  task :upload_shared do
    on roles :all do
      execute :mkdir, "-p #{shared_path}"
      ['shared/config', 'shared/web'].each do |f|
        upload!(f, shared_path, recursive: true)
      end
    end
  end
end

namespace :nginx do
  desc 'create simlink in /etc/nginx/conf.d on nginx.conf application'
  task :append_config do
    on roles :all do
      sudo :ln, "-fs #{shared_path}/config/nginx.conf /etc/nginx/conf.d/#{fetch(:application)}.conf"
    end
  end
  desc 'reload nginx'
  task :reload do
    on roles :all do
      sudo :service, :nginx, :reload
    end
  end
  desc 'restart nginx'
  task :restart do
    on roles :all do
      sudo :service, :nginx, :restart
    end
  end
  after :append_config, :restart
end

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  task :composer do
    on roles(:app) do
      within release_path do
        execute "cd #{release_path} && composer install"
      end
    end
  end

  task :migrate do
    on roles(:app) do
      within release_path do
        #execute "cd #{release_path} && ./yii migrate --interactive=0"
      end
    end
  end

  after :updated, "deploy:composer"
  after :updated, "deploy:migrate"
end