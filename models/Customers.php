<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $birthdate
 * @property string $created_at
 * @property string $updated_at
 * @property integer $client_source_id
 * @property boolean $is_teacher
 * @property string $address_reg
 * @property string $passport_serial
 * @property string $passport_num
 * @property string $passport_issued
 * @property string $passport_issued_date
 * @property string $email
 * @property string $education
 * @property string $occupation
 * @property string $language_skill
 * @property integer $representative_id
 * @property string $birth_doc
 * @property string $full_name
 * @property string $position
 * @property string $position_r
 * @property string $director
 * @property string $director_r
 * @property string $basedoc_r
 * @property string $inn
 * @property string $kpp
 * @property string $ogrn
 * @property string $okpo
 * @property string $okato
 * @property string $account
 * @property integer $bank_id
 * @property string $sign
 * @property string $short_name
 * @property integer $entity_id
 * @property string $exported
 * @property integer $auto_close_interval
 * @property boolean $not_active
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'address_reg', 'education', 'occupation', 'language_skill'], 'string'],
            [['birthdate', 'created_at', 'updated_at', 'passport_issued_date', 'exported'], 'safe'],
            [['created_at', 'updated_at'], 'required'],
            [['client_source_id', 'representative_id', 'bank_id', 'entity_id', 'auto_close_interval'], 'integer'],
            [['is_teacher', 'not_active'], 'boolean'],
            [['name', 'phone', 'passport_serial', 'passport_num', 'passport_issued', 'email', 'birth_doc', 'full_name', 'position', 'position_r', 'director', 'director_r', 'basedoc_r', 'inn', 'kpp', 'ogrn', 'okpo', 'okato', 'account', 'sign', 'short_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'phone' => 'Phone',
            'birthdate' => 'Birthdate',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'client_source_id' => 'Client Source ID',
            'is_teacher' => 'Is Teacher',
            'address_reg' => 'Address Reg',
            'passport_serial' => 'Passport Serial',
            'passport_num' => 'Passport Num',
            'passport_issued' => 'Passport Issued',
            'passport_issued_date' => 'Passport Issued Date',
            'email' => 'Email',
            'education' => 'Education',
            'occupation' => 'Occupation',
            'language_skill' => 'Language Skill',
            'representative_id' => 'Representative ID',
            'birth_doc' => 'Birth Doc',
            'full_name' => 'Full Name',
            'position' => 'Position',
            'position_r' => 'Position R',
            'director' => 'Director',
            'director_r' => 'Director R',
            'basedoc_r' => 'Basedoc R',
            'inn' => 'Inn',
            'kpp' => 'Kpp',
            'ogrn' => 'Ogrn',
            'okpo' => 'Okpo',
            'okato' => 'Okato',
            'account' => 'Account',
            'bank_id' => 'Bank ID',
            'sign' => 'Sign',
            'short_name' => 'Short Name',
            'entity_id' => 'Entity ID',
            'exported' => 'Exported',
            'auto_close_interval' => 'Auto Close Interval',
            'not_active' => 'Not Active',
        ];
    }
}
