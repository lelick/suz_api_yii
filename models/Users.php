<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $email
 * @property string $encrypted_password
 * @property string $reset_password_token
 * @property string $reset_password_sent_at
 * @property string $remember_created_at
 * @property integer $sign_in_count
 * @property string $current_sign_in_at
 * @property string $last_sign_in_at
 * @property string $current_sign_in_ip
 * @property string $last_sign_in_ip
 * @property string $created_at
 * @property string $updated_at
 * @property string $login
 * @property integer $organization_id
 * @property string $timeoffset
 * @property string $avatar
 * @property string $gmail
 * @property string $gpassword
 * @property string $gcalendar
 * @property string $gtoken
 * @property string $name
 * @property boolean $is_client
 * @property integer $customer_id
 * @property integer $date_refresh_time
 * @property boolean $is_admin
 * @property boolean $send_notifications
 * @property boolean $show_milage
 * @property boolean $show_trip
 * @property boolean $show_dinner
 * @property string $phone
 * @property boolean $not_active
 * @property boolean $staff
 * @property string $auth_token
 */
class Users extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reset_password_sent_at', 'remember_created_at', 'current_sign_in_at', 'last_sign_in_at', 'created_at', 'updated_at'], 'safe'],
            [['sign_in_count', 'organization_id', 'customer_id', 'date_refresh_time'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['is_client', 'is_admin', 'send_notifications', 'show_milage', 'show_trip', 'show_dinner', 'not_active', 'staff'], 'boolean'],
            [['email', 'encrypted_password', 'reset_password_token', 'current_sign_in_ip', 'last_sign_in_ip', 'login', 'timeoffset', 'avatar', 'gmail', 'gpassword', 'gcalendar', 'gtoken', 'name', 'phone', 'auth_token'], 'string', 'max' => 255],
            [['auth_token'], 'unique'],
            [['reset_password_token'], 'unique'],
        ];
    }

    public function fields()
    {
        return [
            'id',
            'email_name' => 'email',
            'login',
            'name',
            'name_d' => function () {
                return $this->login . ' ' . $this->name;
            },
            'avatar',
            'is_client',
            'is_admin',
            'phone',
            'staff',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'encrypted_password' => 'Encrypted Password',
            'reset_password_token' => 'Reset Password Token',
            'reset_password_sent_at' => 'Reset Password Sent At',
            'remember_created_at' => 'Remember Created At',
            'sign_in_count' => 'Sign In Count',
            'current_sign_in_at' => 'Current Sign In At',
            'last_sign_in_at' => 'Last Sign In At',
            'current_sign_in_ip' => 'Current Sign In Ip',
            'last_sign_in_ip' => 'Last Sign In Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'login' => 'Login',
            'organization_id' => 'Organization ID',
            'timeoffset' => 'Timeoffset',
            'avatar' => 'Avatar',
            'gmail' => 'Gmail',
            'gpassword' => 'Gpassword',
            'gcalendar' => 'Gcalendar',
            'gtoken' => 'Gtoken',
            'name' => 'Name',
            'is_client' => 'Is Client',
            'customer_id' => 'Customer ID',
            'date_refresh_time' => 'Date Refresh Time',
            'is_admin' => 'Is Admin',
            'send_notifications' => 'Send Notifications',
            'show_milage' => 'Show Milage',
            'show_trip' => 'Show Trip',
            'show_dinner' => 'Show Dinner',
            'phone' => 'Phone',
            'not_active' => 'Not Active',
            'staff' => 'Staff',
            'auth_token' => 'Auth Token',
        ];
    }

    public static function getCurUserId()
    {
        return Yii::$app->user->identity->getId();
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        //return static::findOne(['auth_token' => $token]);
        Tokens::clearExpires();
        
        $tokenModel = Tokens::findOne(['token' => $token]);
        if ($tokenModel){
            $tokenModel->updated_at = 0;
            $tokenModel->save();

            return $tokenModel->user;
        }
        else
            return false;
    }

    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_token;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function generateToken()
    {
        return Yii::$app->security->generateRandomString();
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->encrypted_password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString(64);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Tokens::className(), ['user_id' => 'id']);
    }
}
