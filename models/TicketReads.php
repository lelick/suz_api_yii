<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use Yii;

/**
 * This is the model class for table "ticket_reads".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $ticket_id
 * @property string $created_at
 * @property string $updated_at
 */
class TicketReads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_reads';
    }

    public function behaviors()
    {
        return [
          [
              'class' => TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => 'updated_at',
              'value' => new Expression('NOW() at time zone \'utc\''),
          ],
      ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ticket_id'], 'integer'],
            //[['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function fields()
    {
        return [
            'id',
            'user_id',
            'created_at',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'ticket_id' => 'Ticket ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
