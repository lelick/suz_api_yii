<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tickets".
 *
 * @property integer $id
 * @property string $ticket_date
 * @property integer $user_id
 * @property string $description
 * @property string $planned_date
 * @property integer $manager_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $customer_id
 * @property string $ticket_contact
 * @property integer $parent_id
 * @property integer $rating
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tickets';
    }

     /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_date', 'planned_date', 'created_at', 'updated_at'], 'safe'],
            [['user_id', 'manager_id', 'customer_id', 'parent_id', 'rating'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'required'],
            [['ticket_contact'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_date' => 'Ticket Date',
            'user_id' => 'User ID',
            'description' => 'Description',
            'planned_date' => 'Planned Date',
            'manager_id' => 'Manager ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'customer_id' => 'Customer ID',
            'ticket_contact' => 'Ticket Contact',
            'parent_id' => 'Parent ID',
            'rating' => 'Rating',
        ];
    }

    public function fields()
    {
        return [
            'id',
            'description',
            'ticket_contact',
            'ticket_date',
            'created_at',
            'updated_at',
            'user' => function () {
                return $this->user;
            },
            'manager' => function () {
                return $this->manager;
            },
            'customer' => function () {
                return $this->customer;
            },
            'state' => function () {
                return $this->state;
            },
            'is_read' => function() {
                $userId = Yii::$app->user->identity->getId();
                return $this->getRead($userId);
            }
        ];
    }

    public function getRead($userId)
    {
        foreach ($this->reads as $ticketRead)
        {
            if ($ticketRead->user_id == $userId)
                return true;
        }
        return false;
    }

    /**
     * Make read ticket
     * @return $this
     */

    public function viewTicket()
    {
        $userId = Yii::$app->user->identity->getId();
        if (!self::getRead($userId)){
            $tr = new TicketReads();
            $tr->ticket_id = $this->id;
            $tr->user_id = $userId;
            $tr->save();
        }
        return $this;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Users::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Users::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(TicketLastStates::className(), ['ticket_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStates()
    {
        return $this->hasMany(TicketStates::className(), ['ticket_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReads()
    {
        return $this->hasMany(TicketReads::className(), ['ticket_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Usermessages::className(), ['ticket_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachments::className(), ['ticket_id' => 'id']);
    }
}
