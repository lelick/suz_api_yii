<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use Yii;

/**
 * This is the model class for table "tokens".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $device_id
 * @property string $device_name
 * @property string $token
 * @property integer $expires_at
 * @property string $created_at
 * @property string $updated_at
 */
class Tokens extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tokens';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW() at time zone \'utc\''),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'expires_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['device_id', 'device_name'], 'string', 'max' => 32],
            [['token'], 'string', 'max' => 255],
            [['token'], 'unique'],
        ];
    }

    public static function clearExpires()
    {
        $sql = "updated_at <= current_timestamp at time zone 'utc' - interval '24 hours'";
        //return self::deleteAll('user_id = :user_id AND ' . $sql, [':user_id' => $userId]);
        return self::deleteAll($sql);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'device_id' => 'Device ID',
            'device_name' => 'Device Name',
            'token' => 'Token',
            'expires_at' => 'Expires At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
