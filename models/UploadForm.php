<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 02.11.16
 * Time: 22:05
 */

namespace app\models;


use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class UploadForm extends Model
{

    /**
     * @var UploadedFile
     */
    public $imageFile;


    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
        $uploadPath = Yii::$app->basePath . '/uploads/';

        if ($this->validate()) {
            //$this->imageFile->saveAs($uploadPath . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $this->imageFile->saveAs($uploadPath . uniqid() . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }

}