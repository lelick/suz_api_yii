<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usermessages".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $body
 * @property integer $ticket_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $problem_id
 */
class Usermessages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usermessages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ticket_id', 'problem_id'], 'integer'],
            [['body'], 'string'],
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'body' => 'Body',
            'ticket_id' => 'Ticket ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'problem_id' => 'Problem ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicket()
    {
        return $this->hasOne(Tickets::className(), ['id' => 'ticket_id']);
    }
}
