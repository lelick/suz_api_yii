<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reports".
 *
 * @property integer $id
 * @property integer $ticket_id
 * @property integer $customer_id
 * @property integer $user_id
 * @property integer $manager_id
 * @property integer $organization_id
 * @property string $job_description
 * @property string $since
 * @property string $till
 * @property string $created_at
 * @property string $updated_at
 * @property integer $client_user_id
 * @property string $reason
 * @property string $comment
 * @property integer $milage_start
 * @property integer $milage_end
 * @property integer $trip_time
 * @property integer $dinner_time
 * @property boolean $overwork
 * @property boolean $overwork_auto
 */
class Reports extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'customer_id', 'user_id', 'manager_id', 'organization_id', 'client_user_id', 'milage_start', 'milage_end', 'trip_time', 'dinner_time'], 'integer'],
            [['job_description', 'reason', 'comment'], 'string'],
            [['since', 'till', 'created_at', 'updated_at'], 'safe'],
            [['created_at', 'updated_at'], 'required'],
            [['overwork', 'overwork_auto'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_id' => 'Ticket ID',
            'customer_id' => 'Customer ID',
            'user_id' => 'User ID',
            'manager_id' => 'Manager ID',
            'organization_id' => 'Organization ID',
            'job_description' => 'Job Description',
            'since' => 'Since',
            'till' => 'Till',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'client_user_id' => 'Client User ID',
            'reason' => 'Reason',
            'comment' => 'Comment',
            'milage_start' => 'Milage Start',
            'milage_end' => 'Milage End',
            'trip_time' => 'Trip Time',
            'dinner_time' => 'Dinner Time',
            'overwork' => 'Overwork',
            'overwork_auto' => 'Overwork Auto',
        ];
    }
}
