<?php

require(__DIR__ . '/../../current/vendor/autoload.php');
require(__DIR__ . '/../../current/vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../../current/config/web.php');

(new yii\web\Application($config))->run();